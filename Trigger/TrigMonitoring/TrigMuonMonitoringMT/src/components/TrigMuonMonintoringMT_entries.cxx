/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "../L1MuonMonMT.h"
#include "../L2MuonSAMonMT.h"
#include "../TrigMuonEfficiencyMonMT.h"
#include "../MuonMatchingTool.h"

DECLARE_COMPONENT( TrigMuonEfficiencyMonMT )
DECLARE_COMPONENT( L1MuonMonMT )
DECLARE_COMPONENT( L2MuonSAMonMT )
DECLARE_COMPONENT( MuonMatchingTool )
